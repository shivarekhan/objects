function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    if (obj === undefined || obj === null) {
        return [];
    }
    let resultArr = [];
    for (let i in obj) {
        let tempArr = [];
        tempArr.push(i);
        tempArr.push(obj[i]);
        resultArr.push(tempArr);
    }
    return resultArr;
}

module.exports = pairs;