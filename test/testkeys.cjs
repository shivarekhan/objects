const keys = require('../keys.cjs');
// const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
let keyresult = keys({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
if(keyresult.length ===0){
    console.log("invalid input");
}
else{
    console.log(keyresult);
}

let keyresult1 = keys({ "id": 45, "car_make": "Audi", "car_model": "TT", "car_year": 2008 });
if(keyresult1.length ===0){
    console.log("invalid input");
}
else{
    console.log(keyresult1);
}
    
let keyresult2 = keys({one: 1, two: 2, three: 3});
if(keyresult2.length ===0){
    console.log("invalid input");
}
else{
    console.log(keyresult2);
}

let keyresult3 = keys({ });
if(keyresult3.length ===0){
    console.log("invalid input");
}
else{
    console.log(keyresult3);
}