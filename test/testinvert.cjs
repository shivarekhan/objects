const invert = require('../invert.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result1 = invert({ "id": 45, "car_make": "Audi", "car_model": "TT", "car_year": 2008 });
if (result1 == null || result1 == undefined) {
    console.log("invalid input");
}
else {
    console.log(result1);
}

const result2 = invert(testObject);
if (result2 == null || result2 == undefined) {
    console.log("invalid input");
}
else {
    console.log(result2);
}
