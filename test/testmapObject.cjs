const mapObject = require('../mapObject.cjs');

let result1 = mapObject({start: 5, end: 12},function(value,key){
    return value + 5;
})
if(result1 == null){
    console.log("input object was empty");
}
else{
    console.log(result1);
}


let result2 = mapObject({ },function(value,key){
    return value + 5;
})
if(result2=={}){
    console.log("input object was empty");
}
else{
    console.log(result2);
}
