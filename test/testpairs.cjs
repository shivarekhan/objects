const pairs = require('../pairs.cjs');

const result1 = pairs({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
if (result1.length === 0 || result1 === undefined) {
    console.log("invalid input");
}
else {
    console.log(result1);
}

const result2 = pairs({ });
if (result2.length === 0 || result2 === undefined) {
    console.log("invalid input");
}
else {
    console.log(result2);
}

const result3 = pairs({ "id": 45, "car_make": "Audi", "car_model": "TT", "car_year": 2008 });
if (result3.length === 0 || result3 === undefined) {
    console.log("invalid input");
}
else {
    console.log(result3);
}