const values = require('../values.cjs');
let valuesResult1 = values({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
if(valuesResult1.length ==0){
    console.log("give a valid object");
}
else{
    console.log(valuesResult1);
}

let valuesResult2 = values({});
if(valuesResult2.length ==0){
    console.log("give a valid object");
}
else{
    console.log(valuesResult2);
}

let valuesResult3 = values({ "id": 45, "car_make": "Audi", "car_model": "TT", "car_year": 2008 });
if(valuesResult3.length ==0){
    console.log("give a valid object");
}
else{
    console.log(valuesResult3);
}