function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    if(obj === undefined || obj===null ){
        return [];
    }
    let arr = [];
    for(let i in obj){
        arr.push(obj[i]);
    }
    return arr;

}
module.exports = values;