const keys = require('./keys.cjs');
function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    if (obj == null && defaultProps == null) {
        return [];
    }
    else if (obj == null && defaultProps !== null) {
        return defaultProps;
    }
    else if (obj !== null && defaultProps === null) {
        return obj;
    }
    let keylist = keys(defaultProps);
    for (let i = 0; i < keylist.length; i++) {
        if (!obj[keylist[i]]) {
            obj[keylist[i]] = defaultProps[keylist[i]];
        }
    }
    return obj;

}

module.exports = defaults;