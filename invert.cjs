const keys = require('./keys.cjs');
function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    if (obj.length == 0 || obj == undefined) {
        return [];
    }
    let keyList = keys(obj);
    let resultObj = {};
    for (let i = 0, length = keyList.length; i < length; i++) {
        resultObj[obj[keyList[i]]] = keyList[i];

    }
    return resultObj;
}
module.exports = invert;