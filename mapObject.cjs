const keys = require('./keys.cjs')
function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    if(obj.length === null ){
        return {};
    }
    let keyslist = keys(obj);
    for(let i=0;i<keyslist.length;i++){
        let temp = cb(obj[keyslist[i]],keyslist[i]);
        obj[keyslist[i]] = temp;
    } 
    return obj;
}
module.exports = mapObject;